import os ,sys
from cron_descriptor import Options, CasingTypeEnum, DescriptionTypeEnum, ExpressionDescriptor
import schedule
import json
import time
import datetime
import sonata_monitor
import ssh_connection
import paramiko
import pdb
import logging

"""
Task Scheduler
==========
- This module manages periodic tasks using cron.
- ConvertCronExpression method translate cron expression to human-readable time expression
- CheckFrequency method using schedule module to set scheduler for each work item
- clearSchedule method(optional) - clear all data for scheduling 

# 0 12/1 * * * - #Every 1 hours, starting at 12:00 PM
# 0 12 1/1 * * -  # Every day at 12 pm
"""

try:
    class CronScheduler:

        def Read_rules_file(self):
            with open('C:\\Users\\aviv.cohen\\Desktop\\Monitoring\\ruleConfig.json') as f:
                json_data = json.load(f)
                work_items = json_data['ossConfigurationData']
            return work_items

        def Clear_schedule(self):
            schedule.clear()

        def Convert_cron_expression(self, cron_exp):        
            options = Options()
            options.throw_exception_on_parse_error = True
            options.casing_type = CasingTypeEnum.Sentence
            options.use_24hour_time_format = True
            descripter = ExpressionDescriptor(cron_exp, options)

            return (descripter.get_description(DescriptionTypeEnum.FULL))


        def Check_frequency(self, work_item, monitor_instance, SSH_client, ssh_instance):
            # properties
            frequency = work_item['frequently']
            cron_expresion = self.Convert_cron_expression(work_item['cronInterval'])
            work_item_attributes = work_item['attribute']

            switcher = {
                1: "daily",
                2: "hourly"
            }
            if frequency not in switcher.values():
                print switcher.get(frequency, " - Invalid frequency value")
            elif frequency == 'hourly':
                schedule.every(10).seconds.do(monitor_instance.Create_instances_for_monitoring, 
                work_item_attributes, SSH_client, ssh_instance)            
            elif frequency == 'daily':
                schedule.every(3).minutes.do(monitor_instance.Create_instances_for_monitoring, 
                work_item_attributes, SSH_client, ssh_instance)

            # elif frequency == 'hourly':
            #     interval = int(cron_expresion.split()[7])
            #     start_time = cron_expresion.split()[11][:3]+cron_expresion.split()[1] # Aggregate HH:mm

            #     schedule.every(interval).hours.at(start_time).do(monitor_instance.Create_instances_for_monitoring, 
            #     work_item_attributes, SSH_client, ssh_instance)            
            # elif frequency == 'daily':
            #     data = schedule.every().day.at(cron_expresion.split()[1]).do(monitor_instance.Create_instances_for_monitoring, 
            #     work_item_attributes, SSH_client, ssh_instance)
            # return data            

            # while 1: # Run as a service
            #         schedule.run_pending()
            #         time.sleep(5)


# Procedures
    cron_instance = CronScheduler()    
    monitor_instance = sonata_monitor.Monitor()
    ssh_instance = ssh_connection.RemoteSSHConnection() # initialize instance from RemoteSSHConnection     

    SSH_client = paramiko.SSHClient() # module to various SSH operations
    
    rules_data = cron_instance.Read_rules_file()
    cron_instance.Clear_schedule()
    for key,val in rules_data.iteritems():
        for i in range(len(val)):
            cron_instance.Check_frequency(val[i], monitor_instance, SSH_client, ssh_instance)

    while True:
        schedule.run_pending()
        time.sleep(10)     


except OSError as err:
    logging.basicConfig(filename='exception_log.txt',format='*************************\n %(asctime)s %(levelname)s: %(message)s \n*************************', datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO, exc_info=1)                        
    logging.info("OS error: {0}".format(err))               
    print("OS error: {0}".format(err))







