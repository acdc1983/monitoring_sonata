import sys
import os
import json
import bz2
import paramiko
import logging

"""
RemoteSSHConnection class in charge to SSH various servers with relevant credentials  

- Fetch_SSH_connection method_data fetch connection data to server (ip, host, pwd ....)
- Encode_Pwd & Decoded_pwd methods encrypt passwords in connectionConf.json file 
- Connect_to_remote_Server method perfrom connection to server & execute commands
"""

try:

    class RemoteSSHConnection:

        def Fetch_SSH_connection_data(self):
            with open('C:\\Users\\aviv.cohen\\Desktop\\Monitoring\\connectionConf.json') as f:
                json_data = json.load(f)

            return json_data

        def Encode_Pwd(self, pstPwd):
            encoded_pwd= bz2.compress(pstPwd)

            return encoded_pwd

        def Decoded_pwd(self, pstPwd):
            single_back_slash_password = pstPwd.decode('string_escape')
            decoded_pwd = bz2.decompress(single_back_slash_password)

            return decoded_pwd
        
        def Connect_to_remote_Server(self, data, client, shellCmd):
            ip = data['IP']
            user = data['user']
            passwd = self.Decoded_pwd(data['password']) # Decoding the encrypted pwd
            # pkey=
            # timeout=
            COMMAND = shellCmd
            # client.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # ingonre hostname 
            # client.connect(ip, username=user, password=passwd) # Open the socket
            # ssh_stdin, ssh_stdout, ssh_stderr = client.exec_command(COMMAND, timeout=None)

            # out = ssh_stdout.readlines()
            # err = ssh_stderr.readlines()

            # print out
            # print err

            # client.close()

        # return out, err
            return [[u'12\n'], []] 

except OSError as err:
    logging.basicConfig(filename='exception_log.txt',format='*************************\n %(asctime)s %(levelname)s: %(message)s \n*************************', datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO, exc_info=1)                        
    logging.info("OS error: {0}".format(err))               
    print("OS error: {0}".format(err))
