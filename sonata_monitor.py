import json
import os
import sys
import logging
from prettytable import PrettyTable
import pdb
import datetime

'''
## The following script created to the perform the following actions over Sonata:
1. Monitor
2. Alerting
3. Updating

## The code read the configuration file that contain various attributes:
1. Vendor name
2. Path to file
3. File type
4. Minimun number of of files (mandatory)
5. Maximum number of of files (optional)
6. Search command/s (Linux/ win)
 
# Code support Monitor class to allow various modules for all clients module 
  for all components 
# Python Logger - in order of severity:
    1. Critical (Numeric value: 50)
    2. Warning (Numeric value: 40)
    3. Error (Numeric value: 30)
    4. Info (Numeric value: 20)
    5. Debug (Numeric value: 10)
'''
try:
    class Monitor:        
        
        rule_type_list = ['counts', 'print']

        # def __init__(self):
            # self.json_data = json_data
            # self.listOfWorkItems = listOfWorkItems
            # self.ruleTYpeList = ['counts', 'print']

        def Read_shell_command_file(self, search_key):
            sub_str = "\\"
            with open('C:\\Users\\aviv.cohen\\Desktop\\Monitoring\\shellSearchCommands.json') as f:
                json_data = json.load(f)
                shell_cmd = json_data['searchConfiguration']['items'][0][search_key] 
                if shell_cmd.find(sub_str) != -1:
                    shell_cmd.replace(sub_str, "\\")

            return shell_cmd    

        def Fetch_connection_details(self, connection_id, SSH_connect_Data):
            conn_id = ''
            for i in range(len(SSH_connect_Data['connection'])):
                if SSH_connect_Data['connection'][i]['connectionID'] == connection_id:
                    conn_id = SSH_connect_Data['connection'][i]
                    break
                    
            return conn_id
    
        # Method to create records in Monitor log file. The format is table 
        def Create_table_records_for_log(self, **kwargs):
            table_instance = PrettyTable()
            disk_partition_validation = bool(1)

            is_success = 'Success' if kwargs['is_success'] is True else 'Failure' # set success or failure opt
            rule_type = kwargs['config']['rules'][0]['ruleType']

            if rule_type == "counts":
                table_instance.field_names = ["Time", "Rule ID","IP", "Info", "Min_value", "Max_value", "Actual_value", "Status", "Search_command"]

                table_instance.add_row([
                    datetime.date.strftime(datetime.datetime.now(), "%m/%d/%Y %H:%M:%S"),
                    kwargs['config']['rules'][0]['ruleID'],
                    kwargs['ssh_data']['IP'],
                    '{} - {}'.format(kwargs['config']['action'], kwargs['config']['vendor_name']),
                    kwargs['config']['rules'][0]['minimumNumberOfFile'],
                    kwargs['config']['rules'][0]['maximumNumberOfFile'] if 'maximumNumberOfFile' in kwargs['config']['rules'][0] else 'None',
                    kwargs['search_cmd'],
                    is_success,
                    kwargs['shell_command']
                ])

            elif rule_type == "print": # ATM - fits to printing df -h 
                table_instance =  PrettyTable([])
                maximum_disk_use = 80 # The maximum precentage level in disk partition

                table_instance.field_names = [kwargs['search_cmd'][0]]
                table_instance.align[kwargs['search_cmd'][0]] = "l"

                for i in range(len(kwargs['search_cmd']))[1:]:
                    table_instance.align[kwargs['search_cmd'][i]] = "l"
                    table_instance.add_row([
                        kwargs['search_cmd'][i]
                    ])
                    partition_size = (kwargs['search_cmd'][i].split()[4]).translate(None, '%')
                    if partition_size >= maximum_disk_use:
                            disk_partition_validation = bool(0)

            return table_instance, disk_partition_validation

        def Write_to_log_file(self, **kwargs):
            
            if not kwargs['ssh_data'] or kwargs['config']['rules'][0]['ruleType'] not in self.rule_type_list or 'minimumNumberOfFile' not in kwargs['config']['rules'][0]:
                logging.basicConfig(filename='error_log.txt',
                format='**************************************************\n %(asctime)s %(levelname)s: %(message)s \n**************************************************',
                datefmt='%m/%d/%Y %H:%M:%S \n', level=logging.INFO, exc_info=1)
                # logging.error('%s \n %s', ruleType, logRecord)
                logging.critical(kwargs['log'])
            else:
                if kwargs['config']['rules'][0]['ruleType'] == 'counts':
                    log_record = self.Create_table_records_for_log(config=kwargs['config'], ssh_data=kwargs['ssh_data'], search_cmd=kwargs['search_cmd'], is_success=kwargs['is_success'], shell_command=kwargs['shell_command'])
                elif kwargs['config']['rules'][0]['ruleType'] == 'print':
                    log_record = self.Create_table_records_for_log(config=kwargs['config'], ssh_data=kwargs['ssh_data'], search_cmd=kwargs['search_cmd'], is_success=kwargs['is_success'])

                ### TODO - FIX ORIGINAL LOGGING 
                # logging.basicConfig(filename='log.txt',
                # format='**************************************************\n %(asctime)s %(levelname)s: \n %(message)s \n**************************************************',
                # datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO, exc_info=1)
                # # logging.error('%s \n %s', ruleType, logRecord)
                # logging.info(log_record)

                ### temp - write to different log to avoid redundant text in log.txt file                

                date_fmt=datetime.date.strftime(datetime.datetime.now(), "%m/%d/%Y %H:%M:%S")
                level_of_severity = logging.getLevelName(logging.INFO)
                str_title = '\n\n**************************\n' + date_fmt+ " *" + level_of_severity  + '*\n*********************************************************************************************************************'
                log_record_Str = str(log_record[0])
                with open("sonata_monitor_log.txt", "a") as f:
                    if kwargs['config']['rules'][0]['ruleType'] == 'counts':
                        f.write(str_title + '\n' + log_record_Str)
                    elif kwargs['config']['rules'][0]['ruleType'] == 'print':
                        IP = kwargs['ssh_data']['IP']
                        server_name = kwargs['ssh_data']['name']
                        cmd = kwargs['shell_command']
                        disk_valid = 'Success' if log_record[1] is True else 'Failure' # set success or failure opt

                        f.write(str_title + '\n' + '{} \t IP:{}({}) \t STATUS: {}'.format(cmd, IP, server_name, disk_valid) + '\n' + log_record_Str)
                    f.close()

        def Run_search_command(self, configuration_attr, rule_type_list, search_command_output, SSH_connect_data, shell_command):
            rule_type = configuration_attr['rules'][0]['ruleType']            
            
            if rule_type in rule_type_list:
                if rule_type == 'counts':
                    if 'minimumNumberOfFile' not in configuration_attr['rules'][0]:                        
                        component = configuration_attr['inputDataFilePath'].format(configuration_attr['name'])
                        log='Minimum amount of files for {} is not specifide in configuration'.format(component)
                        self.Write_to_log_file(config=configuration_attr, ssh_data=SSH_connect_data,
                         log=log, is_success=bool(0))
                    else:
                        minimum_number_of_file = configuration_attr['rules'][0]['minimumNumberOfFile']
                
                        if 'maximumNumberOfFile' not in configuration_attr['rules'][0]:
                            if int(search_command_output) != int(minimum_number_of_file):
                                self.Write_to_log_file(config=configuration_attr, ssh_data=SSH_connect_data, search_cmd=search_command_output, is_success=bool(0), shell_command=shell_command)    
                            else:
                                self.Write_to_log_file(config=configuration_attr, ssh_data=SSH_connect_data, search_cmd=search_command_output, is_success=bool(1), shell_command=shell_command)
                        else: # maximum_number_Of_file has value
                            maximum_number_Of_file = configuration_attr['rules'][0]['maximumNumberOfFile']
                            if (int(search_command_output) < int(minimum_number_of_file)) or (int(search_command_output) > int(maximum_number_Of_file)):
                                self.Write_to_log_file(config=configuration_attr, ssh_data=SSH_connect_data, search_cmd=search_command_output, is_success=bool(0), shell_command=shell_command)
                            else:# Output from server holds in count minimun & maximum range 
                                self.Write_to_log_file(config=configuration_attr, ssh_data=SSH_connect_data, search_cmd=search_command_output, is_success=bool(1), shell_command=shell_command)
                                         
                elif rule_type == 'print':
                    self.Write_to_log_file(config=configuration_attr, ssh_data=SSH_connect_data, search_cmd=search_command_output, is_success=bool(1), shell_command=shell_command)
            else: # Rule not exist                
                self.Write_to_log_file(config=configuration_attr, ssh_data=SSH_connect_data, log='No Rule type Name {} exist in process'.format(rule_type), is_success=bool(0))
                

        def Create_instances_for_monitoring(self, attributes, SSH_client, ssh_instance):
            SSH_connect_data = ssh_instance.Fetch_SSH_connection_data() # Read SSH connection configuration file
            for i in range(0, len(attributes)):
                configuration_attr = attributes[i]

                server_Connection_Data = self.Fetch_connection_details(configuration_attr['connectionID'], SSH_connect_data)
                if not server_Connection_Data: # No remote server 
                    self.Write_to_log_file(ssh_data='', log='No SSH connection to connectionID:{}'.format(configuration_attr['connectionID']), is_success=bool(0))
                else:
                    search_key = configuration_attr['rules'][0]['search']
                    input_Data_file_path = configuration_attr['inputDataFilePath']
                    shell_command = self.Read_shell_command_file(search_key).format(input_Data_file_path)
                    if not shell_command:
                        self.Write_to_log_file(ssh_data='', log='cannot find search command for rule ID:{}'.format(configuration_attr['rules'][0]['ruleID']), is_success=bool(0))
                    else:
                        value_from_remote_server = ssh_instance.Connect_to_remote_Server(server_Connection_Data, SSH_client, shell_command)
                        value_from_remote_server = value_from_remote_server[0][0] if configuration_attr['rules'][0]['ruleType']=='counts' else value_from_remote_server[0]
                        
                        self.Run_search_command(configuration_attr, self.rule_type_list, 
                        value_from_remote_server, server_Connection_Data, shell_command)


except OSError as err:
    logging.basicConfig(filename='exception_log.txt',format='*************************\n %(asctime)s %(levelname)s: %(message)s \n*************************', datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO, exc_info=1)                        
    logging.info("OS error: {0}".format(err))               
    print("OS error: {0}".format(err))
